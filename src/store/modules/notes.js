import noteJson from '../../data/notes.json'

const state = {
    notes: noteJson.notes
};

export default {
    state
}