import alertJson from '../../data/alerts.json';
import notesJson from '../../data/notes.json' ;
import Vue from 'vue'

const state = {
    alerts: alertJson.alerts,
    notes: notesJson.notes,
    noteMountedAlerts: null,
    alertsInfoForCalendar: null
}

const getters = {

    mountNotesToAlerts: state => {

        console.log(state)
        let copyOfAlertsArray = state.alerts.slice();
        state.noteMountedAlerts = copyOfAlertsArray.map(( alert ) => {
            alert.notes = [];
            state.notes.map(( note ) => {
                if( alert.id === note.alertId ){
                    alert.notes.push( note.note );
                }
            });
            return alert;
        });
    },
    prepareAlertInfoForCalendar: state => {

        let calendarData = {};

        for(let key in state.alerts) {

            let date = new Date(state.alerts[key].createdAtTimestamp).getDate();
            if( !calendarData.hasOwnProperty(date) ) {

                calendarData[date] = [];
                calendarData[date].push(state.alerts[key]);
            }else {

                calendarData[date].push(state.alerts[key]);
            }
        }

        state.alertsInfoForCalendar = calendarData;
    }

}

let mutations = {

    addNoteToAlert: ( state, data ) => {

        state.noteMountedAlerts.filter( note => {
            return note.id === data.alert.id;
        }).map( note => {
            note.notes.push(data.text);
        })

    }
}

let actions = {
    addNoteToAlertAction: ({ commit }, data ) => {
        commit('addNoteToAlert', data)
    }
}
export default {
    state,
    getters,
    mutations,
    actions
}
