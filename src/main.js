

require('./assets/fonts/Blender/BlenderProBold/BlenderPro-Bold.otf')
require('./assets/fonts/Blender/BlenderProHeavy/BlenderPro-Heavy.otf')
require('./assets/fonts/Blender/BlenderProMedium/BlenderPro-Medium.otf')
import Vue from 'vue'
import App from './App.vue'
import { store } from './store/store';
import VueRouter from 'vue-router';
import { routes } from "./routes";


Vue.use(VueRouter);
const router = new VueRouter({
    routes,
    mode: 'history'
})

new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
})
