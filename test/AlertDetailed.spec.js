import { shallowMount } from '@vue/test-utils'
import AlertDetailed from '@/components/AlertDetailed.vue'
import { store } from '@/store/store'
describe( 'AlertDetailed', () => {

    let wrapper;
    let alert;

    beforeEach( () => {

        store.getters.mountNotesToAlerts;
        alert = store.state.alerts.noteMountedAlerts[0];

        wrapper = shallowMount( AlertDetailed, {
            propsData: {
                alert
            },
            store
        });
    })

    it( 'gets alert to render', () => {

        expect( wrapper.vm.alert ).toBe( alert );
    })

    it( 'clicks to add new button and triggers click event and checks new note is added.', () => {

        jest.spyOn( wrapper.vm, 'onAddNewNote');
        wrapper.find( 'button' ).trigger( 'click' );
        wrapper.find( 'textArea' ).element.value = 'metiner';
        wrapper.findAll( 'button' ).at(1).trigger('click');
        expect( wrapper.vm.onAddNewNote ).toBeCalled();
        expect( wrapper.vm.$props.alert.notes.length).toBe(4);
    })
})