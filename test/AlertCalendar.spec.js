import { shallowMount, mount } from '@vue/test-utils'
import AlertCalendar from '@/components/AlertCalendar.vue'

describe( 'AlertCalendar', () => {

    it( 'renders grid', () =>{

        let wrapper = mount( AlertCalendar ,{
            stubs: {
                'app-alert-calendar-detailed': '<div></div>'
            }
        });

        expect( wrapper.vm.$el.getElementsByClassName('row').length ).toBe(5);
        expect( wrapper.vm.$el.getElementsByClassName('col').length ).toBe(35);

    })
})