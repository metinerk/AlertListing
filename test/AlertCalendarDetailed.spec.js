import { shallowMount }  from '@vue/test-utils'
import AlertCalendarDetailed from '@/components/AlertCalendarDetailed.vue'
import { store } from '@/store/store'

describe( 'AlertCalendarDetailed', () => {


    store.getters.prepareAlertInfoForCalendar;
    let alerts = store.state.alerts.alertsInfoForCalendar;

    let wrapper = shallowMount( AlertCalendarDetailed, {
        propsData: {
            dayCount: 25,
            alerts
        },
        store
    });

    it( 'receives dayCount number', () => {

        expect( wrapper.vm.dayCount ).toBe(25)
    })
})