import { shallowMount } from '@vue/test-utils'
import Header from '@/components/Header.vue'

describe( 'Header', () => {
    it( 'is loading image', () => {

        let wrapper = shallowMount( Header )

        expect( wrapper.vm.$el.querySelector('img').getAttribute('src')).toBe('../assets/images/logo.svg');
        expect( wrapper.vm.$el.querySelectorAll('div').length).toBe(1);
    })
})