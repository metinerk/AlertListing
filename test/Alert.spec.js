import { shallowMount } from '@vue/test-utils'
import Alert from '@/components/Alert.vue'
import alerts from '@/data/alerts.json'

describe( 'Alert', () => {

    it( 'gets alert and clicks it', () => {

        let alert = alerts.alerts[0];
        let index = 1;

        const wrapper = shallowMount( Alert, {

                propsData: { alert , index }
            }
        );

        jest.spyOn( wrapper.vm, 'toDetailedAlert' );
        wrapper.find( '.row' ).trigger( 'click' );

        expect( wrapper.vm.alert ).toBe( alert );
        expect( wrapper.vm.index ).toBe( index );
        expect( wrapper.vm.toDetailedAlert ).toBeCalled();
    })
})