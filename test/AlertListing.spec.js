import { shallowMount } from '@vue/test-utils'
import AlertListing from '@/components/AlertListing.vue'
import { store }  from '@/store/store'

describe('AlertListing', () => {

    it('expect alert values are loaded', () => {

        const wrapper = shallowMount( AlertListing, {

            store
        });

        wrapper.vm.$store.getters.mountNotesToAlerts;
        wrapper.vm.alerts = wrapper.vm.$store.state.alerts.noteMountedAlerts;
        expect( wrapper.vm.alerts.length ).toBeGreaterThan(0)
    })
})